-Breve introducción a la aplicación

Para poder desarrollar el ejercicio solicitado utilicé el framework de React.JS. El cual cuenta con un menú donde se puede acceder a una vista inicial del proyecto y a la Api de datos. 

Si bien no tengo amplia experiencia en el framework, traté de utilizar varios recursos que he estado aprendiendo.
Se que algunas cosas se pueden mejorar, plantearlas mejor así que sigo trabajando en poder progresar día a día.

-Listado de las tecnologías o frameworks utilizados

Al framework de React le sume REDUX, donde cree un store para tener una mejor organización de los datos, las llamadas a las funciones y controlar mejor la aplicación.
Dentro de React utilicé algunas dependencias externas como React - Router.

Para la maquetación y algunos estilos del sitio usé BOOTSTRAP, la cual trabajo a diario con y ella y tiene muchas clases propias muy útiles así como su sistema de gryd que mejoran
el responsive de la APP. 
También utilicé una librería de w3schools-w3css para dar un sutil dinamismo a los componentes y así mejorar la experiencia del usuario.

Para la tarea de copiar el color usé una librería externa llamada "clipboard.js" la cual consumo desde cdn.

-Cómo instalar las dependencias y correr el proyecto

Adjunto todos los archivos del proyecto, los cuales se encuentran en un repositorio público de gitlab - https://gitlab.com/araggi/frontend-prueba. Es necesario contar con React.