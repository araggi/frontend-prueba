import React, {Fragment, useState} from 'react'
import {useDispatch} from 'react-redux'
import {Link } from "react-router-dom";
import Titulo from './Titulo';

import {obtenerDataAccion} from '../redux/apiDucks'

const Ficha = () => {
    // dispatch
    const dispatch = useDispatch()

    // useState
    const [colorInfo] = useState([])

    return (
        <Fragment>
            <Titulo />
            <div className="row">
                <div className="col-md-12">
                <div className="card text-center" key={colorInfo.id}>
                    <div className="card-header text-left">
                        {colorInfo.year}
                    </div>
                    <div className="card-body pt-5 pb-5">
                        
                        <h1 className="card-title">¡ Copiado !</h1>
                    </div>
                    <div className="card-footer text-right">
                        {colorInfo.pantone_value}
                    </div>
                    </div>
                </div>

                <div className="col-md-12 text-center pt-4">
                    <Link to="/reduxcolor">
                        <span className="nav-link" className="btn btn-info text-light w3-animate-bottom" onClick={ () => dispatch(obtenerDataAccion()) }>Volver</span>
                    </Link>
                </div>
            </div>
        </Fragment>
    )
}

export default Ficha

