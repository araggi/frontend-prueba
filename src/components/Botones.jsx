import React, {Fragment} from 'react'
import {useDispatch, useSelector} from 'react-redux'

import {siguientePaginaAccion, prevPaginaAccion} from '../redux/apiDucks'

const Botones = () => {
    
    const dispatch = useDispatch()
    const id_pag = useSelector( store => store.colores.page )
    const sin_data = useSelector( store => store.colores.data.length )

    return (
        <Fragment>
            <div className="row">
                <div className="col-md-6 text-left">
                {
                        id_pag > 1 && // muestro a partir de la página 2
                        <button className="btn btn-info text-light w3-animate-left" onClick={ () => dispatch(prevPaginaAccion())}>Anterior</button>
                }                       
                </div>
                <div className="col-md-6 text-right">
                {   

                        id_pag >= 1 && sin_data !== 0 ? // si no hay contenido en el array no se muetra
                        <button className="btn btn-info text-light w3-animate-right" onClick={ () => dispatch(siguientePaginaAccion())}>Siguiente</button>
                        : ''
                }
                    
                </div>
            </div>
        </Fragment>
    )
}

export default Botones
