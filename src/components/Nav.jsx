import React, {Fragment} from 'react'

const Nav = () => {
    return (
        <Fragment>
                <div className="col-md-12 text-center">
                <hr></hr>
                <nav aria-label="Page navigation example">
                    <ul className="pagination justify-content-center">
                        <li className="page-item disabled">
                        <a className="page-link" href="#" >Previous</a>
                        </li>
                        <li className="page-item">
                        <a className="page-link" href="#">Next</a>
                        </li>
                    </ul>
                    </nav>
                </div>
        </Fragment>
    )
}

export default Nav

