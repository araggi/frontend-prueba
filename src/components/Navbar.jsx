import React, {Fragment} from 'react'
import {Link} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux'
import {obtenerDataAccion} from '../redux/apiDucks'


const Navbar = () => {
    const dispatch = useDispatch()

    const id_pag = useSelector( store => store.colores.page )
    const sin_data = useSelector( store => store.colores.data.length )
    return (
        <Fragment>
        <nav className="navbar navbar-expand-sm bg-info navbar-dark">
        <ul className="navbar-nav mx-auto">
            <li className="nav-item active mr-5">
                <a className="nav-link " href="/">Presentación</a>
            </li>
            <li className="nav-item">
            {
                        id_pag >= 3 && sin_data === 0 ?
                        '' : (
                            <Link to="/reduxcolor">
                                <span className="nav-link active "  onClick={ () => dispatch(obtenerDataAccion()) }>Obtener Api Colores</span>
                            </Link>
                        )
                    }
            </li>

        </ul>
        </nav>
        </Fragment>
    )
}

export default Navbar
