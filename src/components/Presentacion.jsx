import React, {Fragment} from 'react'

const Presentacion = () => {

    return (
        <Fragment>
            <div className="row">
                <div className="col-md-12 text-center pt-5 w3-container w3-center w3-animate-zoom">
                    <h1>Reto: <strong>Front End Developer</strong></h1>
                    <p className="pt-2">Crea un proyecto en el framework front end que más te guste o que pienses que es lo más óptimo para solucionar este problema.</p>
                </div>
            </div>
        </Fragment>
    )
}

export default Presentacion
