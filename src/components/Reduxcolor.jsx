import React, {Fragment} from 'react'

import {useDispatch, useSelector} from 'react-redux'
import {obtenerDataAccion} from '../redux/apiDucks'
import Botones from './Botones'
import Titulo from './Titulo'


const Reduxcolor = () => {

    const dispatch = useDispatch()
    const dataColor = useSelector( store => store.colores.data )
    const id_pag = useSelector( store => store.colores.page )
    const sin_data = useSelector( store => store.colores.data.length )
    
    return (
        <Fragment>
        
        <Titulo />
        <div className="row">
            <div className="container-fluid p-5">
                <div className="row justify-content-center text-center">
                    <div className="card-columns col-md-12 text-center pt-3 " >
                    
                    {
                        id_pag >= 3 && sin_data === 0 ? // sin datos en la api
                        <div className="card-body">
                            <h6 className="text-center">Sin datos actualizados desde la API</h6>
                        </div>
                        
                        :(

                        dataColor.map( item =>(
                            
                                <div className="card text-center border-secondary w3-animate-zoom" key={item.id}>
                                <div className="card-header text-left">
                                    {item.year}
                                </div>
                                <div className="card-body">
                                    <p className="card-text text-capitalize font-weight-bold">{item.name}</p>
                                    <a href={`/ficha/${item.id}`} className="btn btn-default">
                                        <span className="btnss" data-clipboard-text={item.color} onClick={ () => dispatch(obtenerDataAccion()) } >{item.color} </span>
                                    </a>
                                </div>
                                <div className="card-footer text-right font-italic">
                                    {item.pantone_value}
                                </div>
                                </div>
                                
                            
                        )) )
                    }
                    </div>
                    </div>
                    <hr></hr>
                    <Botones />
                </div>
        </div>
        </Fragment>
    )
}

export default Reduxcolor

 
  