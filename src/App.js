import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink
} from "react-router-dom";

import { Provider } from 'react-redux';
import generateStore from './redux/store'
import Reduxcolor from './components/Reduxcolor';
import Presentacion from './components/Presentacion';
import Ficha from './components/Ficha';
import Navbar from './components/Navbar';


function App() {

  const store = generateStore()

  return (
    <Provider store={store}>
      <Router>
        <Navbar />
      <div className="container-fluid">
          <Switch>
          <Route path="/" exact>
            <Presentacion />
          </Route>
          </Switch>

        <Switch>
          <Route path="/reduxcolor">
            <Reduxcolor />
          </Route>
        </Switch>
        <Switch>
            <Route path="/ficha/:ids">
              <Ficha />
          </Route>
        </Switch>
      </div>
      </Router>
    </Provider>
  );
}

export default App;
