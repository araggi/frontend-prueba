import axios from 'axios'

// const 
const DataInicial = {
    data: [],
    page : 1,
}

// type
const OBTENER_DATA = 'OBTENER_DATA'
const GET_NEXT_DATA = 'GET_NEXT_DATA'
const PREVIOUS_DATA = 'PREVIOUS_DATA'

// redux
export default function colordata (state = DataInicial, action){
    switch (action.type) {
        case OBTENER_DATA:
            return {...state, data: action.payload}

        case GET_NEXT_DATA:
            return {...state, data: action.payload.data, page: action.payload.page}

        case PREVIOUS_DATA:
            return {...state, ...action.payload}
        default:
            return state
    }
}


export const obtenerDataAccion = () => async (dispatch, getState) =>{

    // Guardo los primeros datos de la API en un localStore

    if(localStorage.getItem('colores')){
        console.log('datos guardados')
        dispatch({
            type: OBTENER_DATA,
            payload: JSON.parse(localStorage.getItem('colores'))
        })
        return
    }

    const page = getState().colores.page
    
    try {
        console.log('datos de la api')
        const res = await axios.get(`https://reqres.in/api/colors/?page=${page}`)
        dispatch({
            type: OBTENER_DATA,
            payload: res.data.data
        })
        localStorage.setItem('colores', JSON.stringify(res.data.data)) // transformo el objeto en un cadena
    } catch (error) {
        console.log(error)
    }

}

export const siguientePaginaAccion = () => async (dispatch, getState) =>{

    const page = getState().colores.page
    const siguiente = page + 1

    try {
        const res = await axios.get(`https://reqres.in/api/colors/?page=${siguiente}`)
        
        dispatch({
            type: GET_NEXT_DATA,
            payload: {
                data: res.data.data,
                page: siguiente
            }
        })
        
    } catch (error) {
        console.log(error)
    }
}

export const prevPaginaAccion = () => async (dispatch, getState) =>{

    const page = getState().colores.page
    const prev = page - 1

    try {
        const res = await axios.get(`https://reqres.in/api/colors/?page=${prev}`)
        
        dispatch({
            type: GET_NEXT_DATA,
            payload: {
                data: res.data.data,
                page: prev
            }
        })
        
    } catch (error) {
        console.log(error)
    }
}