

import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk' // para hacer promesas

import apiReducer from './apiDucks'

const rootReducer = combineReducers({
    colores: apiReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore(){
    const store = createStore(rootReducer, composeEnhancers( applyMiddleware(thunk) ))
    return store;
}